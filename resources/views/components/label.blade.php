@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-xl xl:text-md 2xl:text-lg text-gray-700']) }}>
    {{ $value ?? $slot }}
</label>