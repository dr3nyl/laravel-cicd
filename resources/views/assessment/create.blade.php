<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Create Assessment') }}
        </h2>
    </x-slot>

    <div class="dark:bg-gray-800 bg-white shadow-md rounded px-8 pt-6 pb-5 mb-4 flex flex-row my-2 max-w-7xl mx-auto mt-10">
        <div class="mr-6">
            <div class="-mx-3 md:flex mb-3">
                <div class="md:w-1/2 px-3 mb-6 md:mb-0 flex flex-row">
                    <div class="my-8 mr-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold" for="email" :value="__('Manager')" />
                    </div>
                    <div class="flex flex-col items-center">
                        <div>
                            <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('SESAID')" />
                        </div>
                        <div>
                            <input class="md:w-[11rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-red rounded py-1 px-4 mb-3" id="grid-first-name" type="text" placeholder="Jane">
                        </div>
                    </div>

                    <div class="flex flex-col items-center px-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('Name')" />
                        <input class="md:w-[18rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-grey-lighter rounded py-1 px-4" id="grid-last-name" type="text" placeholder="Doe">
                    </div>

                    <div class="flex flex-col items-center px-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('Position')" />
                        <input class="md:w-[20rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-grey-lighter rounded py-1 px-4" id="grid-last-name" type="text" placeholder="Doe">
                    </div>
                </div>
            </div>
            <div class="-mx-3 md:flex mb-4">
                <div class="md:w-1/2 px-3 mb-6 md:mb-0 flex flex-row">
                    <div class="my-8 mr-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold" for="email" :value="__('Employee')" />
                    </div>
                    <div class="flex flex-col items-center">
                        <div>
                            <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('SESAID')" />
                        </div>
                        <div>
                            <input class="md:w-[11rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-red rounded py-1 px-4 mb-3" id="grid-first-name" type="text" placeholder="Jane">
                        </div>
                    </div>

                    <div class="flex flex-col items-center px-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('Name')" />
                        <input class="md:w-[18rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-grey-lighter rounded py-1 px-4" id="grid-last-name" type="text" placeholder="Doe">
                    </div>

                    <div class="flex flex-col items-center px-2">
                        <x-input-label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email" :value="__('Position')" />
                        <input class="md:w-[20rem] appearance-none block w-full bg-grey-lighter text-grey-darker lg:text-sm border border-grey-lighter rounded py-1 px-4" id="grid-last-name" type="text" placeholder="Doe">
                    </div>
                </div>
            </div>
        </div>
        <div class="text-[14px] bg-red-300 p-3">
            <header><strong> The scale of assessment ranges from 1 to 5 </strong></header>
            <div>
                <ol class="text-[12px]">
                    <li>1. Novice: Demonstrate capability but with very limited on-the-job experience(ie new on the job)</li>
                    <li>2. Basic: Demonstrate capability in routine/basic situations</li>
                    <li>3. Competent: Demonstrate capability in various situations</li>
                    <li>4. Advanced: Demonstrate capability in complex situations and advise others in this area</li>
                    <li>5. Expert: Demonstrate exemplary capability and/or design best practice methodology and processes</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="dark:bg-gray-800 bg-white shadow-md rounded px-8 pt-6 pb-5 mb-4 my-2 max-w-7xl mx-auto">
        <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
            <ul class="flex flex-wrap -mb-px text-sm font-medium text-center" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                <li class="mr-2" role="presentation">
                    <button class="inline-block p-4 border-b-2 rounded-t-lg" id="profile-tab" data-tabs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Understand Customer & Business</button>
                </li>
                <li class="mr-2" role="presentation">
                    <button class="inline-block p-4 border-b-2 rounded-t-lg" id="dashboard-tab" data-tabs-target="#dashboard" type="button" role="tab" aria-controls="dashboard" aria-selected="false">Drive Business for Growth</button>
                </li>
                <li class="mr-2" role="presentation">
                    <button class="inline-block p-4 border-b-2 rounded-t-lg" id="settings-tab" data-tabs-target="#settings" type="button" role="tab" aria-controls="settings" aria-selected="false">Aim for Profitability</button>
                </li>
            </ul>
        </div>
        <div id="myTabContent">
            <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <p class="text-sm text-gray-500 dark:text-gray-400">This is some placeholder content the <strong class="font-medium text-gray-800 dark:text-white">Profile tab's associated content</strong>. Clicking another tab will toggle the visibility of this one for the next. The tab JavaScript swaps classes to control the content visibility and styling.</p>
            </div>
            <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
                <p class="text-sm text-gray-500 dark:text-gray-400">This is some placeholder content the <strong class="font-medium text-gray-800 dark:text-white">Dashboard tab's associated content</strong>. Clicking another tab will toggle the visibility of this one for the next. The tab JavaScript swaps classes to control the content visibility and styling.</p>
            </div>
            <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                <p class="text-sm text-gray-500 dark:text-gray-400">This is some placeholder content the <strong class="font-medium text-gray-800 dark:text-white">Settings tab's associated content</strong>. Clicking another tab will toggle the visibility of this one for the next. The tab JavaScript swaps classes to control the content visibility and styling.</p>
            </div>
            <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="contacts" role="tabpanel" aria-labelledby="contacts-tab">
                <p class="text-sm text-gray-500 dark:text-gray-400">This is some placeholder content the <strong class="font-medium text-gray-800 dark:text-white">Contacts tab's associated content</strong>. Clicking another tab will toggle the visibility of this one for the next. The tab JavaScript swaps classes to control the content visibility and styling.</p>
            </div>
        </div>
    </div>
</x-app-layout>
